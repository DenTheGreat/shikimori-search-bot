Телеграм бот для поиска по сайту shikimori.org.

 Требования:
 
 - ```python 3.6+``` 
 
 Для развёртки проекта:
 
 - Создать среду ```$ virtualenv env``` 
 
 - Активировать среду ```$ source env/bin/activate```
  
 - Установить зависимости ```$ pip install -r requirements.txt```
 
 - Поставить свой токен бота в ```main.py```
 
 - Запустить скрипт ```$ python main.py```
