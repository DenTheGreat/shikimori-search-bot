from scrapy import Spider
from scrapy.crawler import CrawlerProcess, CrawlerRunner
import scrapy.crawler as crawler
from twisted.internet import reactor


class QuotesSpider(Spider):

    def __init__(self,data, category='', **kwargs):
        self.start_urls = [f'https://shikimori.org/animes?search={category}']
        self.data = data
        super().__init__(**kwargs)
            
    name = 'jojo'
    def parse(self, response):
        for quote in response.css('article'):
            obj =  {
                'name': quote.css('span.name-en ::text').get(),
                'misc': quote.css('span.misc span ::text').getall(),
                'link': quote.css('a ::attr(href)').get(),
            }
            self.data.append(obj)
            yield obj

def search(keyword,result):
    runner = CrawlerRunner()
    data = []
    d = runner.crawl(QuotesSpider, data, category=keyword)
    d.addBoth(lambda _: reactor.stop())
    reactor.run()
    print("Search data:",data)
    print("Search passed result:",result)
    result['result'] = data
    print("Search final result:",result)