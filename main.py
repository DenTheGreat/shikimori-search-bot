import logging
import os
import sys
from telegram.ext import *
from telegram import *
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',level=logging.INFO)
logger = logging.getLogger(__name__)

from multiprocessing import Process, Manager
from shiki_spider import search

def run_spider(keyword):
    if __name__ == '__main__':
        manager = Manager()
        result = manager.dict()
        p = Process(target=search, args=(keyword,result,))
        p.start()
        p.join()
        print("Run_spider data:",result)
        return result

updater = Updater(token='token') #Токен к боту

def search_command(bot,update, args):
    keyword = " ".join(args)
    data = run_spider(keyword)
    print(data)
    text = ''
    for i in data['result']:
        try:
            text += '👉 ' + i['name'] + ' ' + i['misc'][0] + ' ' + i['misc'][1] + ' https://shikimori.org' + i['link'] + '\n'
        except:
            text += "\n"
    if text == '':
        text = 'Ничего не найдено 🤷‍♂️'
    bot.send_message(chat_id=update.message.chat_id, text=text)
    #stop_and_restart()

def main():
    global updater
    bot = updater.bot
    dispatcher = updater.dispatcher
    # Добавляем хендлеры в диспетчер
    dispatcher.add_handler(CommandHandler("shiki", search_command, pass_args=True))
    # Начинаем поиск обновлений
    print("Жду Ctrl+C")
    updater.start_polling(clean=True)
    updater.idle()

if __name__ == '__main__':
    main()